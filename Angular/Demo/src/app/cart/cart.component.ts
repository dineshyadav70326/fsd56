import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {

  cartItems: any;
  cartProducts: any;

  constructor(private service: EmpService) {
    this.cartItems = service.getCartItems();
  }

  ngOnInit() {
    //this.cartProducts = localStorage.getItem("cartItems");
    //this.cartItems = JSON.parse(this.cartProducts);
  }

}