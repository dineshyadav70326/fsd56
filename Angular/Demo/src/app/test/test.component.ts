import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {


  id: number;
  name: string;
  avg: number;
  address: any;
  hobbies: any;


  constructor() {
    this.id = 101;
    this.name = "hasrsha";
    this.avg = 45.56
    this.address = {
      streetno: 101,
      city: 'Hydrrabad',
      state: 'Telanagana'
    };

    this.hobbies = ['sleeping','eating','music','movies'];

    // alert("consturctor invoked");
  }
  ngOnInit() {
    // alert("ngOnit invoked");
  }
}
